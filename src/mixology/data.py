import numpy as np
from numpy.random import default_rng
import yaml

def drink_search(max_ing, chosen, ing_list, drink_list, menu=dict()):
	if len(chosen) == max_ing:
		possible = []
		# Compute all drinks that can be made with these ingredients.
		for drink in drink_list:
			makeable = True
			for ing in drink['ingredients']:
				if ing['ingredient'] not in chosen: makeable = False
			if makeable: possible.append(drink['name'])

		return chosen, possible

	else:
		best_ing, best_drinks = [], []
		for ing in ing_list:
			if ing in chosen: continue
			if ing < chosen[-1]: continue
			cbest_ing, cbest_drinks = drink_search(max_ing, chosen+[ing], ing_list, drink_list, menu)
			if len(cbest_drinks) >= len(best_drinks): 
				best_ing, best_drinks = cbest_ing, cbest_drinks
				l = len(best_drinks)
				#if not l in menu: menu[l] = set()
				#menu[l].add(frozenset(best_drinks))
		return best_ing, best_drinks

def parse_data():
	ingredients = set()
	by_ing = {}
	by_name = {}
	ing_index = {}
	index_ing = {}
	drink_list = []

	for f in ["data/pdt.yaml","data/matthew.yaml", "data/core.yaml"]:
		with open(f) as file:
			count = 0
			local_drink_list = yaml.load(file, Loader=yaml.FullLoader)
			drink_list.extend(local_drink_list)
			for drink in local_drink_list:
				by_name[drink['name']] = drink
				for ingredient in drink['ingredients']:
					name = ingredient['ingredient']
					ingredients |= set((name,))
					if not name in by_ing: by_ing[name] = []
					by_ing[name].append(drink['name'])
				count += 1
	ing_index = {k:v for v,k in enumerate(ingredients)}
	index_ing = {v:k for v,k in enumerate(ingredients)}
	return ingredients, by_ing, by_name, ing_index, index_ing, drink_list