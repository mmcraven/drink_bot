import math
import sys

import yaml

import mixology.data
import mixology.evolve

free = ["Water"]
forced = ['Orange juice', 'Champagne', 'Tequila']#['Orange juice', 'Champagne']
max_drinks = 6

def filter(ing, menu, drink_list):
	save = []
	for drink in drink_list:
		if drink['name'] in menu: save.append(drink)
	#with open('menu.yaml', 'w+') as outfile:
		#yaml.dump(save, outfile)
	return save

def evolve_menu():
	ingredients, by_ing, by_name, ing_index, index_ing, drink_list = mixology.data.parse_data()
	possible_menus = set()
	for item in mixology.evolve.evolve_menu(max_drinks, forced, free, ing_index, index_ing, drink_list):
		names = [index_ing[idx] for idx in item]+forced+free
		_, menu = mixology.data.drink_search(max_drinks+len(free),names, ingredients, drink_list)
		possible_menus.add(frozenset(menu))

	options = {}
	for idx, menu in enumerate(possible_menus):
		ing = set()
		for drink in menu:
			for ingredient in by_name[drink]['ingredients']: 
				ing.add(ingredient['ingredient'])
		ing.update(free)
		ing.update(forced)
		i, m = mixology.data.drink_search(max_drinks+len(free), list(ing), ingredients, drink_list)
		options[idx] = (i,m)
		print(f"Menu {idx}:")
		print("Ingredients:",i,"\nDrinks:",m,"\n\n")
	choice = int(input("Choice or -1 if to run exit without saving: "))
	if choice == -1: sys.exit(0)
	return *options[choice], drink_list
def convert_to_motors(menu):
	ing_to_motor = {}
	for drink in menu:
		for ingredient in drink['ingredients']:
			ingredient = ingredient['ingredient']
			if ingredient in free: continue
			elif ingredient in ing_to_motor: continue
			else: ing_to_motor[ingredient] = len(ing_to_motor)
	# Use sentinel value of -1 to indicate no motor mapping
	for x in free: ing_to_motor[x] = -1

	converted_menu = []
	for drink in menu:
		fixed_ingredients = []
		for ingredient in drink['ingredients']:
			name = ingredient['ingredient']
			amt = 0
			if ingredient['unit'] == "cl": amt = math.ceil(10*ingredient['amount'])
			elif ingredient['unit'] == 'ml': amt = math.ceil(ingredient['amount'])
			else: assert 0
			fixed_ingredients.append({'ingredient':name, 'unit': 'ml', 'group': ing_to_motor[name], 
				"amount": amt})
		fixed_drink = {"name": drink["name"], 'ingredients':fixed_ingredients}
		converted_menu.append(fixed_drink)

	return converted_menu
def print_mapping(converted_menu):
	mapping = {}
	for drink in converted_menu:
		for ingredient in drink['ingredients']: mapping[ingredient['ingredient']] = ingredient['group']
	print("Please ensure the motors are set up as follows:")
	for (k,v) in mapping.items(): 
		if v == -1: print(f"Have {k} on hamd, but not connected to a motor.")
		else: print(f"Motor {v} pumping ingredient {k}.")

def main():
	ing, selected_drinks, drink_list = evolve_menu()
	menu = filter(ing, selected_drinks, drink_list)
	motorized = convert_to_motors(menu)
	#print(motorized)
	print_mapping(motorized)
	with open('menu.yaml', 'w+') as outfile:
		yaml.dump(motorized, outfile)
	# save motor assignment, drink list so that it can be read later

if __name__ == "__main__":
	main()
