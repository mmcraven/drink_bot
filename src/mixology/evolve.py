import random
from deap import base
from deap import creator
from deap import tools

class score:
	def __init__(self, max_drinks, forced, free, ing_index, index_ing, drink_list):
		self.ing_index = ing_index
		self.index_ing = index_ing
		self.forced = forced
		self.free = free
		self.drink_list = drink_list
	def __call__(self, ind):
		global index_ing
		possible = 0 
		chosen = [self.index_ing[i] for i in ind]+self.forced+self.free
		# Compute all drinks that can be made with these ingredients.
		for drink in self.drink_list:
			makeable = True
			for ing in drink['ingredients']:
				if ing['ingredient'] not in chosen: makeable = False
			if makeable: possible += 1

		return possible,

def evolve_menu(max_drinks, forced, free, ing_index, index_ing, drink_list):

	creator.create("FitnessMax", base.Fitness, weights=(1.0,))
	creator.create("Individual", list, fitness=creator.FitnessMax)



	toolbox = base.Toolbox()

	toolbox.register("attr_int", random.randint, 0, len(ing_index)-1)
	toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_int, n=max_drinks-len(forced))
	toolbox.register("population", tools.initRepeat, list, toolbox.individual)
	def feasible(individual): return len(set(individual)) == len(individual)
	def distance(individual): return len(individual) - len(set(individual)),

	toolbox.register("mate", tools.cxTwoPoint)
	toolbox.register("mutate", tools.mutUniformInt, low=0, up=len(ing_index)-1, indpb=0.05)
	toolbox.register("select", tools.selTournament, tournsize=3)
	toolbox.register("evaluate", score(max_drinks, forced, free, ing_index, index_ing, drink_list))
	toolbox.decorate("evaluate", tools.DeltaPenalty(feasible, -2*max_drinks, distance))
	pop = toolbox.population(n=300)
	CXPB, MUTPB, NGEN = 0.5, 0.2, 500

	# Evaluate the entire population
	fitnesses = map(toolbox.evaluate, pop)
	for ind, fit in zip(pop, fitnesses):
		ind.fitness.values = fit

	for g in range(NGEN):
		# Select the next generation individuals
		offspring = toolbox.select(pop, len(pop))
		# Clone the selected individuals
		offspring = list(map(toolbox.clone, offspring))

		# Apply crossover and mutation on the offspring
		for child1, child2 in zip(offspring[::2], offspring[1::2]):
			if random.random() < CXPB:
				toolbox.mate(child1, child2)
				del child1.fitness.values
				del child2.fitness.values

		for mutant in offspring:
			if random.random() < MUTPB:
				toolbox.mutate(mutant)
				del mutant.fitness.values

		# Evaluate the individuals with an invalid fitness
		invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
		fitnesses = map(toolbox.evaluate, invalid_ind)
		for ind, fit in zip(invalid_ind, fitnesses):
			ind.fitness.values = fit

		# The population is entirely replaced by the offspring
		pop[:] = offspring

	return pop