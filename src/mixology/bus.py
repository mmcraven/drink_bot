from smbus2 import SMBus, i2c_msg


def clamp(num, small, large): return max(small, min(num, large))
def wait_until_done(address):
	print("Pouring......\n\n")
	# TODO: Wait here until drink is poured.
	print("Done pouring. Enjoy!")
def pour_drink(addr, motor_amount):
	"""
	Take in (motor #, amount in ML) pairs, and tell the MoCo to pour that combo.
	"""
	try:
		with SMBus(1) as bus:
			# Write the amount (in ML! to the correct)
			for (group, amt) in motor_amount: bus.write_byte_data(addr, group+2, clamp(amt,0, 255))
	except FileNotFoundError:
		for (group, amt) in motor_amount: print(f"Pouring {amt} ml of group {group}")
