

import mixology.data
import mixology.evolve





if __name__ == "__main__":
	free = ['Champagne']
	forced = ['Light Rum', 'Orange juice']#['Orange juice', 'Champagne']
	max_drinks = 6

	ingredients, by_ing, by_name, ing_index, index_ing, drink_list = mixology.data.parse_data()
	#print(drink_list)
	possible_menus = set()
	for item in mixology.evolve.evolve_menu(max_drinks, forced, free, ing_index, index_ing, drink_list):
		names = [index_ing[idx] for idx in item]+forced+free
		_, menu = mixology.data.drink_search(max_drinks+len(free),names, ingredients, drink_list)
		possible_menus.add(frozenset(menu))

	for menu in possible_menus:
		print(menu)
		ing = set()
		for drink in menu:
			for ingredient in by_name[drink]['ingredients']: 
				ing.add(ingredient['ingredient'])
		ing.update(free)
		ing.update(forced)
		print(mixology.data.drink_search(max_drinks+len(free), list(ing), ingredients, drink_list))