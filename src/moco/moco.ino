#include <Wire.h>
#include <SoftwareSerial.h>

struct regp
{
  bool available, changed;
  uint8_t rp;
} rp;

#define N_MTR 6
struct motors
{
  const float flow_rate[N_MTR] = {2.70, 2.70, 2.70, 2.70, 2.70, 2.70};
  decltype(millis()) start[N_MTR] = {0, 0, 0, 0, 0, 0};
  bool active[N_MTR] = {false, false, false, false, false, false};
  uint8_t pins[N_MTR] = {4, 3, 2, 7, 6, 5};
  uint8_t amt[N_MTR];
} motors;


SoftwareSerial mySerial(13, 12); // RX, TX
void setup()
{
  Wire.begin(4);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register event
  mySerial.begin(9600);
  for(int it=0; it<N_MTR; it++) {
    digitalWrite(motors.pins[it], 1);
    pinMode(motors.pins[it], OUTPUT);
    digitalWrite(motors.pins[it], 1);
  }
}

void loop()
{
  for(int it=0; it<N_MTR; it++) {
    if(!motors.active[it]) continue;
    auto elapsed_time = (millis() - motors.start[it])/1000;
    float poured = motors.flow_rate[it] * elapsed_time;
    if(poured > motors.amt[it]){
      digitalWrite(motors.pins[it], 1);
      motors.active[it] = false;
      motors.amt[it] = 0;
    }
  }
}

void receiveEvent(int howMany)
{
  
  while(Wire.available()) {
    if(Wire.available()) {
      rp.available = true;
      rp.rp = Wire.read() - 2;
    }
    else {
      rp.available = false;
      break;
    }
    if(Wire.available()) {
      auto amt = Wire.read();
      motors.amt[rp.rp] = amt;
      motors.start[rp.rp] = millis();
      motors.active[rp.rp] = true;
      digitalWrite(motors.pins[rp.rp], 0);
      rp.changed = true;
    }
  }
  
}
