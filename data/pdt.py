import math
import re

import pandas as pd
import yaml

df = pd.read_csv("pdt.csv")
drinks = {}
ingredients = set()
ing_count = {}
replace = {}

replace['Cruzan Black Strap Rum'] = 'Dark Rum'
replace['Freshly Brewed Coffee'] = 'Coffee'
replace['Lustau Palo Cortado Sherry'] = 'Sherry'
replace['Al Wadi Pomegranate Molasses'] = 'Molasses'
replace['Chamomile-Infused Compass Box Asyla Blended Scotch Whisky'] = 'Scotch Whisky'

replace['Orange Juice'] = 'Orange Juice'
replace['Angostura Orange Bitters'] = 'Angostura Bitters'
replace['Liquiteria Coconut Water'] = 'Coconut water'
replace['.*Orange Bitters'] = 'Orange bitters'
replace['Lemon Juice'] = 'Lemon juice'
replace['Mount Gay Eclipse Amber Rum'] = 'Dark Rum'
replace['Grand Marnier'] = 'Grand Marnier'
replace['.*Sweet Vermouth'] = 'Sweet Vermouth'
replace['.*Dry Vermouth'] = 'Dry Vermouth'
replace['.*Irish Whiskey'] = 'Irish Whiskey'
replace['.*Tequila'] = 'Tequila'
replace['.*Gin'] = 'Gin'
replace['.*Bourbon'] = 'Bourbon'
replace['.*Scotch Whisky'] = 'Scotch Whisky'
replace['.*Scotch Whiskey'] = 'Scotch Whisky'
replace['.*Rye Whiskey'] = 'Rye Whiskey'
replace['.*Vodka'] = 'Vodka'
replace['.*Sake'] = 'Sake'
replace['.*Cognac'] = 'Cognac'
replace['.*Rose'] = 'Rosé'
replace['.*Champagne'] = 'Champagne'
replace['.*Creme de Cacao'] = 'Creme de Cacao'
replace['.*Milk'] = 'Milk'
replace['.*Grapefruit Juice'] = 'Grapefruit juice'
replace['.*Absinthe'] = "Absinthe"
replace['.*Pisco'] = 'Pisco'
replace['.*Prosecco'] = 'Prosecco'
replace['.*Creme de Cassis'] = 'Creme de Cassis'
replace['.*Green Chartreuse'] = 'Green Chartreuse'
replace['.*Yellow Chartreuse'] = 'Yellow Chartreuse'
replace['.*Cachaca'] = "Cachaca"
replace['Cointreau'] = 'Triple Sec'
replace['George Dickel No. 12 Tennessee Whisky'] = 'Bourbon Whiskey'
replace['Clear Creek Pear Brandy'] = "Pear Brandy"
replace['Honey Syrup'] = 'Honey'
replace['House Grenadine'] = 'Grenadine'
replace['Glen Thunder Corn Whiskey'] = 'Corn Whiskey'
replace["Lemon Hart Overproof Rum"] = "151 Rum"
replace["Banks 5 Island Rum"] = "Dark Rum"
replace['Flor de Cana Silver Dry Rum'] = "Light Rum"
replace['Havana Club 7-Year-Old Rum'] = "Dark Rum"
replace['Lustau Pedro Ximenez Sherry'] = 'Sherry'
replace['Lakewood Cranberry Juice'] = 'Cranberry Juice'
replace['Pampero Aniversario Rum'] = 'Dark Rum'
replace['Martinique Sugar Cane Syrup'] = 'Simple Syrup'
replace["Gosling's Black Seal Rum"] = 'Dark Rum'
replace['Neisson Rhum Blanc'] = 'Light Rum'
replace['Dolin Blanc Vermouth'] = 'Sweet Vermouth'
replace['Club Soda'] = 'Club soda'
replace['Lustau Manzanilla Sherry'] = 'Sherry'
replace['Barbancourt 8-Year-Old Rhum'] = 'Dark Rum'
replace['Schonauer Apfel Schnapps'] = "Apple Schnapps"
replace['Nikka Taketsuru 12-Year-Old Japanese Malt Whisky'] = "Japanese Whisky"
replace['Yamazaki 12-Year-Old Japanese Single Malt Whisky'] = "Japanese Whisky"
replace['Popcorn-Infused Flor de Cana Silver Dry Rum'] = 'Light Rum'
replace['Lustau East India Sherry'] = 'Sherry'
replace['J.M. Rhum Blanc'] = 'Light Rum'
replace['Sugar Cube'] = 'Simple Syrup'
replace['Channing Daughters Scuttlehole Chardonnay'] = 'Chardonay'
replace['Paumanok Cabernet Franc'] = 'Red wine'
replace['Ron Zacapa Centenario 23 Rum'] = 'Dark Rum'
replace['Luxardo Amaretto'] = 'Amaretto'
replace['Barbancourt Rhum Blanc'] = 'Light Rum'
replace['Clear Creek Plum Brandy'] = 'Plum Brandy'
replace["Myers's Dark Rum"] = "Dark Rum"
replace['Bacardi 8 Rum'] = 'Dark Rum'
replace['Ice-Cold Filtered Water'] = 'Water'
replace['El Dorado 15-Year-Old Rum'] = 'Dark Rum'
replace['Lime Juice'] = 'Lime juice'
replace['Kahlua'] = 'Kahlua'
replace['Cranberry Syrup'] = 'Cranberry juice'
replace['Benedictine'] = 'Benedictine'
replace['Mathilde Pear Liqueur'] = 'Pear Liqueur'
replace["Gran Duque D'Alba Brandy de Jerez"] = 'Brandy'
replace['Appleton Estate Reserve Rum'] = 'Dark Rum'
replace['Coca-Cola Classic'] = 'Coke'
replace['Eurovanille Vanilla Syrup'] = 'Vanilla Syrup'
replace["Dow's Ruby Port"] = "Port"
replace['Drambuie'] = "Drambuie"
replace['Angostura Bitters'] = 'Angostura Bitters'
replace['Aperol'] = 'Aperol'
replace['Beleza Pura Cachaca'] = 'Cachaca'
replace['9th Street Alphabet City Coffee Concentrate'] = "Coffee concentrate"
replace['van Oosten Batavia Arrack'] = 'Arrack'
replace['Neisson Rhum Reserve Speciale'] = 'Dark Rum'
replace['Marie Brizard Orange Curacao'] = 'Curaco'
replace['Red Jacket Orchards Apple Cider'] = 'Apple cider'
replace['Martini Bianco Vermouth'] = 'Sweet Vermouth'
replace["Laird's Bonded Apple Brandy"] = 'Apple Brandy'
replace["Rhum Clement V.S.O.P"] = "Dark Rum"
replace["Galliano L'Autentico"] = "Galliano"
replace['Noval Black Port'] = 'Port'
replace["Laird's Applejack"] = "Apple cider"
replace['Cynar'] = 'Cynar'
replace['Punt e Mes'] = 'Sweet Vermouth'
replace['Simple Syrup'] = 'Simple syrup'




for index, row in df.iterrows():
	if row['Drink'] not in drinks: drinks[row['Drink']] = []
	if row['IngredientUse'] != "Build": continue
	drinks[row['Drink']].append((row['Ingredient'], row['Amount']))
	
d_items = ['.* Bitters', 'St. Dalfour Fig Jam', 'Lemongrass Syrup', 'Bonne Maman Raspberry Preserves', 'Hot Water',
'Red Jacket Orchards Apple Butter', 'Bonne Maman Strawberry Preserves', 'Boiron Passion Fruit Puree', 
"Deep Mountain Grade B Maple Syrup", "Pimm's No. 1 Cup", 'Lemon Syrup', 'Strega', 'Nonino Amaro', 'Ricard Pastis', 
'Rhum Clement Creole Shrubb', 'Creme Yvette', 'Pernod', 'Fernet Branca', 'Pineapple Juice', 'Suze', 
'Quince Shrubb (Huilerie Beaujolaise Vinaigre de Coing)', 'Rothman & Winter Creme de Violette', "Trader Tiki's Don's Mix",
'Campari', 'Sombra Mezcal', 'Amaro Ciociaro', 'Blood Orange Juice', 'Spiced Sorrel', 'Black Cardamom Syrup', 'Amer Picon',
'Watermelon Juice', 'Bonne Maman Orange Marmalade', 'Krogstad Aquavit', 'Monteverdi Nocino', 'Trimbach Framboise',
'Tonic Syrup', 'Ciaco Bella Coconut Sorbet', 'Belle de Brillet', 'Lillet Blanc', 'Anchor Genevieve',
'Agave Syrup', 'Lavender Tincture', 'Demerara Syrup', 'Luxardo Bitter', 'Linie Aquavit', 'Kassatly Chtaura Orgeat', 
'Dubonnet Rouge', 'Gran Classico Bitter', 'Luxardo Maraschino Liqueur', 'Lime Cordial', 'Cherry Heering', 
"Blandy's Sercial Madeira", 'Lillet Rouge', 'Barenjager Honey Liqueur', 'Clear Creek Kirschwasser', 
'Freshly Brewed Chamomile Tea', 'Concord Shrubb', 'Tamarind Puree', 'Carlshamns Flaggpunch', 'Mathilde Peche',
'Bols Genever', 'Horchata', 'Pickled Ramp Brine', 'Demerara Sugar', 'Tokaji Aszu 5 Puttonyos "Red Label"', 'Lime Disc',
'St. Elizabeth Allspice Dram', 'Kumquat Syrup', 'Borsci Sambuca', 'Godiva Original Liqueur', 'St. Germain Elderflower Liqueur',
'Rothman & Winter Orchard Apricot', 'Honeydew Melon Juice', 'Maraska Maraschino Liqueur', 'Agave Nectar', 
"John D. Taylor's Velvet Falernum", 'Pama Pomegranate Liqueur']

# Purge drinks using banned ingredients
dd = set()
for reg in d_items:
	for part in drinks: 
		for (k,v) in drinks[part]:
			if k == 'Angostura Bitters': continue
			elif k == "Orange Bitters": continue
			elif re.match(reg, k):  dd.add(part)
for d in dd: del drinks[d]

def rep(item):
	for reg in replace:
		if re.match(reg, item): return replace[reg]
	assert 0

def oz_to_cl(value): return round(2.95735 * value,1)
def dash_to_ml(value): return round(.92 * value,0)
def tsp_to_ml(value): return 5*value
def convert(amt):
	if amt.endswith('oz'): return oz_to_cl(float(amt[0:-2])), 'cl'
	elif amt.endswith("dash"): return dash_to_ml(float(amt[0:-4])),"ml"
	elif amt.endswith("dashes"): return dash_to_ml(float(amt[0:-6])),"ml"
	elif amt.endswith("barspoon"): return tsp_to_ml(float(amt[0:-8])), "ml"
	print(amt)
	return 0, None
as_yaml = []
for drink in drinks:
	local_drink = {}
	local_drink['name'] = drink
	local_drink['ingredients'] = []
	for ing, amt in drinks[drink]:
		ing = rep(ing)
		amt, unit = convert(amt)
		if unit == None: print(drink)
		local_ing = {'ingredient':ing, 'amount':amt, 'unit':unit }
		local_drink['ingredients'].append(local_ing)
	as_yaml.append(local_drink)

with open('pdt.yaml', 'w+') as outfile:
    yaml.dump(as_yaml, outfile)