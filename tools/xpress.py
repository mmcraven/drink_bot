import argparse
import curses
from os import getgrouplist

import yaml

import mixology.bus




def get_drink_list(filename):
	with open("menu.yaml") as file: return yaml.load(file, Loader=yaml.FullLoader)
def count_motors(drink_list):
	ing = set()
	for drink in drink_list:
		for ings in drink['ingredients']:
			if ings['group'] == -1: continue
			else: ing.add(ings['ingredient'])
	return len(ing)

def menu_pour(address, drink_list):
	idx_to_drink = {}
	print("Available drinks are:")
	for idx, drink in enumerate(drink_list):
		idx_to_drink[idx] = drink
		print(f"({idx}) {drink['name']}")
	idx = int(input("Which drink would you like to pour (-1 to cancel): "))
	if idx == -1: return

	# Pour the drink
	pour_list = []
	for ing in idx_to_drink[idx]['ingredients']:
		print(ing)
		group, amount = ing['group'], ing['amount']
		if group == -1: continue
		pour_list.append((group, amount))

	mixology.bus.pour_drink(address, pour_list)
	mixology.bus.wait_until_done(address)


def run_all(address, motor_count, amt=125):
	pour_list = []
	for group in range(motor_count):
		pour_list.append((group, amt))
	mixology.bus.pour_drink(address, pour_list)
	mixology.bus.wait_until_done(address)


def menu_set_motor(address, motor_count):
	group = int(input(f"Choose a group 0..{motor_count}: "))
	amt = int(input(f"Choose an amount 0..255 (ml): "))
	mixology.bus.pour_drink(address, [(group, amt)])
	mixology.bus.wait_until_done(address)

menu_prompt = """Options:
pour (d)rink
(s)et motor
(p)rime / clear pumps
(q)uit
"""
def main(args):
	drink_list = get_drink_list(args.menu)
	motor_count = count_motors(drink_list)
	while True:
		ch = input(menu_prompt)
		if ch == 'q': return
		if ch == 'd': menu_pour(args.address, drink_list)
		if ch == 'p': run_all(args.address, motor_count)
		if ch == 's': menu_set_motor(args.address, motor_count)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Handle I2C.')
	parser.add_argument('--menu', type=str, default="menu.yaml", help='A motor augmented menu.')
	parser.add_argument('--address', type=int, default=4, help='I2c address of motor controller.')

	args = parser.parse_args()
	main(args)